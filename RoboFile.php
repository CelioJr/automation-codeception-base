<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
/*class RoboFile extends \Robo\Tasks
{
    // define public methods as commands
}*/

require_once 'vendor/autoload.php';

class Robofile extends \Robo\Tasks
{
    use \Codeception\Task\MergeReports;
    use \Codeception\Task\SplitTestsByGroups;

    public function parallelSplitTests()
    {	
    	 // Slip your tests by files
        $this->taskSplitTestFilesByGroups(5)
            ->projectRoot('.')
            ->testsFrom('tests/acceptance')
            ->groupsTo('tests/_data/paracept_')
            ->run();
    }

    public function parallelRun()
    {
	    $parallel = $this->taskParallelExec();
	    for ($i = 1; $i <= 2; $i++) {            
	        $parallel->process(
	            $this->taskCodecept() // use built-in Codecept task
	            ->suite('acceptance')
	            ->configFile("codeception.yml")
	           	->env("env{$i}") // run acceptance tests
	            //->group("p$i")        // for all p* groups
	            ->xml("tests/result_$i.xml") // save XML results
	        );
	    }
	    return $parallel->run();

    }

    public function parallelMergeResults()
    {
         $merger = $this->taskMergeXmlReports();

        for ($i = 1; $i <= 2; $i++) {
            $fileName = __DIR__."/tests/_output/tests/_log/result_{$i}.xml";
            if (file_exists($fileName)) {
                $merger->from($fileName);
            }
        }

        $merger->into(__DIR__."/tests/_output/tests/_log/report.xml")->run();

      /*  $merge = $this->taskMergeXmlReports();
        for ($i=1; $i<=2; $i++) {
            $merge->from(__DIR__."/_output/tests/_log/result_{$i}.xml");
        }
        $merge->into("_output/tests/_log/result_paracept.xml")->run();*/
/*        $this->taskMergeXmlReports()
    	->from('_output/tests/result_1.xml')
    	->from('_output/tests/result_2.xml')
    	->into('_output/tests/merged.xml')
    	->run();*/
    }
}